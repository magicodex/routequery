package com.retaiyang.routetable;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

public class PathNodeTest {

  @Test
  public void testAdd() {
    PathNode root = new PathNode();
    PathInfo info = createPathInfo();
    root.add(new String[] { "", "1", null, "3" }, 1, info);
    root.add(new String[] { "", "1", "2", "3" }, 1, info);

    // /1
    {
      PathNode node = getPathNode(root, "1");
      Assert.assertNotNull(node);
      Assert.assertEquals(2, node.getNextNodeMap().size());
      Assert.assertNotNull(node.getNextNodeMap().get(null));
      Assert.assertNotNull(node.getNextNodeMap().get("2"));
    }

    // /1/null
    {
      PathNode node = getPathNode(root, "1", null);
      Assert.assertNotNull(node);
      Assert.assertEquals(1, node.getNextNodeMap().size());
      Assert.assertNotNull(node.getNextNodeMap().get("3"));
    }

    // /1/2
    {
      PathNode node = getPathNode(root, "1", "2");
      Assert.assertNotNull(node);
      Assert.assertEquals(1, node.getNextNodeMap().size());
      Assert.assertNotNull(node.getNextNodeMap().get("3"));
    }

    // /1/null/3
    {
      PathNode node = getPathNode(root, "1", null, "3");
      Assert.assertNotNull(node);
      Assert.assertEquals(0, node.getNextNodeMap().size());
    }

    // /1/2/3
    {
      PathNode node = getPathNode(root, "1", "2", "3");
      Assert.assertNotNull(node);
      Assert.assertEquals(0, node.getNextNodeMap().size());
    }
  }

  @Test
  public void testGet() {
    PathNode root = new PathNode();
    PathNode node1 = new PathNode(new HashMap<String, PathNode>(), new PathInfo(null, "/1", null));
    PathNode node2 = new PathNode(new HashMap<String, PathNode>(), new PathInfo(null, "/1/null", null));
    PathNode node3 = new PathNode(new HashMap<String, PathNode>(), new PathInfo(null, "/1/2", null));
    PathNode node4 = new PathNode(new HashMap<String, PathNode>(), new PathInfo(null, "/1/null/3", null));
    PathNode node5 = new PathNode(new HashMap<String, PathNode>(), new PathInfo(null, "/1/2/3", null));

    root.getNextNodeMap().put("1", node1);
    node1.getNextNodeMap().put(null, node2);
    node1.getNextNodeMap().put("2", node3);
    node2.getNextNodeMap().put("3", node4);
    node3.getNextNodeMap().put("3", node5);

    // /1
    {
      PathInfo info = root.get(new String[] { "1" }, 0);
      Assert.assertNotNull(info);
      Assert.assertEquals("/1", info.getValue());
    }

    // /1/null
    {
      PathInfo info = root.get(new String[] { "1", "b" }, 0);
      Assert.assertNotNull(info);
      Assert.assertEquals("/1/null", info.getValue());
    }

    // /1/2
    {
      PathInfo info = root.get(new String[] { "1", "2" }, 0);
      Assert.assertNotNull(info);
      Assert.assertEquals("/1/2", info.getValue());
    }

    // /1/null/3
    {
      PathInfo info = root.get(new String[] { "1", "b", "3" }, 0);
      Assert.assertNotNull(info);
      Assert.assertEquals("/1/null/3", info.getValue());
    }

    // /1/2/3
    {
      PathInfo info = root.get(new String[] { "1", "2", "3" }, 0);
      Assert.assertNotNull(info);
      Assert.assertEquals("/1/2/3", info.getValue());
    }
  }

  /**
   * 返回路径节点
   * 
   * @param pathNode 路径节点
   * @param pathSegments 路径片段
   * @return 路径节点
   */
  private PathNode getPathNode(PathNode pathNode, String... pathSegments) {
    PathNode currentNode = pathNode;

    for (int i = 0; currentNode != null && i < pathSegments.length; i++) {
      currentNode = currentNode.getNextNodeMap().get(pathSegments[i]);
    }

    return currentNode;
  }

  /**
   * 创建路径信息
   * 
   * @return 路径信息
   */
  private PathInfo createPathInfo() {
    PathInfo pathInfo = new PathInfo(null, null, null);
    return pathInfo;
  }

}
