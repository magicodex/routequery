package com.retaiyang.routetable.testdependency.sample;

/**
 * 
 * 样例组
 *
 */
public class SampleGroup {
  private String path;
  private Sample[] samples;

  public SampleGroup() {

  }

  public SampleGroup(String path, Sample[] samples) {
    this.path = path;
    this.samples = samples;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Sample[] getSamples() {
    return samples;
  }

  public void setSamples(Sample[] samples) {
    this.samples = samples;
  }

}
