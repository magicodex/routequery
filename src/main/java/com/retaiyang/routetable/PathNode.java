package com.retaiyang.routetable;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * 路径节点
 *
 */
public class PathNode {
  /** 下个路径节点 */
  private Map<String, PathNode> nextNodeMap;
  /** 路径信息 */
  private PathInfo pathInfo;

  public PathNode() {
    this.nextNodeMap = new HashMap<String, PathNode>();
    this.pathInfo = null;
  }

  public PathNode(Map<String, PathNode> nextNodeMap, PathInfo pathInfo) {
    this.nextNodeMap = nextNodeMap;
    this.pathInfo = pathInfo;
  }

  public Map<String, PathNode> getNextNodeMap() {
    return nextNodeMap;
  }

  public PathInfo getPathInfo() {
    return pathInfo;
  }

  /**
   * 添加路径
   * 
   * @param pathSegments 路径片段
   * @param index 索引
   * @param pathInfo 路径信息
   */
  public void add(String[] pathSegments, int index, PathInfo pathInfo) {
    if (pathSegments == null) {
      throw new IllegalArgumentException("pathSegments null");
    }

    if (index >= pathSegments.length) {
      if (this.pathInfo != null) {
        throw new RuntimeException("path exists");
      }

      this.pathInfo = pathInfo;
      return;
    }

    String segment = pathSegments[index];
    PathNode node = nextNodeMap.get(segment);

    // 添加节点
    if (node == null) {
      node = new PathNode();
      nextNodeMap.put(segment, node);
    }

    // 添加路径
    node.add(pathSegments, index + 1, pathInfo);
  }

  /**
   * 返回路径信息
   * 
   * @param pathSegments 路径片段
   * @param index 索引
   * @return 路径信息
   */
  public PathInfo get(String[] pathSegments, int index) {
    if (pathSegments == null) {
      throw new IllegalArgumentException("pathSegments null");
    }

    if (index >= pathSegments.length) {
      return pathInfo;
    }

    String segment = pathSegments[index];
    PathNode node = nextNodeMap.get(segment);
    PathInfo info = null;

    if (node != null) {
      info = node.get(pathSegments, index + 1);
    }

    if (info == null) {
      // null代表路径参数
      node = nextNodeMap.get(null);

      if (node != null) {
        info = node.get(pathSegments, index + 1);
      }
    }

    return info;
  }

}
