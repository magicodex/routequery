package com.retaiyang.routetable;

/**
 * 
 * 路径信息
 *
 */
public class PathInfo {
  /** 路径表达式 */
  private String expression;
  /** 路径对应的值 */
  private Object value;
  /** 路径参数名，元素不是null则是路径参数名否则不是路径参数 */
  private String[] pathParameterNames;

  public PathInfo(String expression, Object value, String[] pathParameterNames) {
    this.expression = expression;
    this.value = value;
    this.pathParameterNames = pathParameterNames;
  }

  public String getExpression() {
    return expression;
  }

  public Object getValue() {
    return value;
  }

  public String[] getPathParameterNames() {
    return pathParameterNames;
  }

}
