package com.retaiyang.routetable;

/**
 * 
 * 路由表
 *
 */
public class Routes {
  /** 路径根节点 */
  private PathNode root;

  public Routes() {
    root = new PathNode();
  }

  /**
   * 添加路径
   * 
   * @param path 路径
   * @param value 路径对应的值
   */
  public void add(String path, Object value) {
    if (path == null) {
      throw new IllegalArgumentException("path null");
    }

    if (value == null) {
      throw new IllegalArgumentException("value null");
    }

    // 获取路径片段
    String[] pathSegments = getPathSegments(path);
    String[] pathParameterNames = new String[pathSegments.length];
    String pathSegment = null;
    String pathParameterName = null;

    // 获取路径参数名
    for (int i = 1; i < pathSegments.length; i++) {
      pathSegment = pathSegments[i];
      pathParameterName = getPathParameterName(pathSegment);

      if (pathParameterName != null) {
        pathSegments[i] = null;
        pathParameterNames[i] = pathParameterName;
      }
    }

    PathInfo pathInfo = new PathInfo(path, value, pathParameterNames);
    // 添加路径
    root.add(pathSegments, 1, pathInfo);
  }

  /**
   * 返回匹配的结果
   * 
   * @param path 路径
   * @return 匹配的结果
   */
  public Route get(String path) {
    if (path == null) {
      throw new IllegalArgumentException("path null");
    }

    String[] pathSegments = getPathSegments(path);
    PathInfo pathInfo = root.get(pathSegments, 1);

    if (pathInfo == null) {
      return null;
    }

    Route route = new Route(pathInfo, pathSegments);
    return route;
  }

  /**
   * 返回路径片段
   * 
   * @param path 路径
   * @return 路径片段，第一个路径片段的索引是1
   */
  protected String[] getPathSegments(String path) {
    if (path == null) {
      throw new IllegalArgumentException("path null");
    }

    String[] pathSegments = path.split("/", -1);
    return pathSegments;
  }

  /**
   * 返回路径参数名
   * 
   * @param pathSegment 路径片段
   * @return 路径参数名，不是路径参数则返回null
   */
  protected String getPathParameterName(String pathSegment) {
    if (pathSegment == null) {
      throw new IllegalArgumentException("pathSegment null");
    }

    String pathParameterName = null;

    // 路径参数以":"开头
    if (pathSegment.startsWith(":")) {
      pathParameterName = pathSegment.substring(1);
    }

    return pathParameterName;
  }

}
