支持路径参数的路由表
==================================================

### 该库说明 ###
支持路径参数的路由表，可用于web请求路径匹配。由于V1.0.0版本采用的路由策略复杂，因此改用新的实现方式。

### 该插件支持的 java 版本 ###
```
java1.6+（包括1.6）
```

### 编译代码 ###
```
编译代码
mvn -DskipTests install

执行测试
mvn test
```

### 示例代码 ###
```
Routes routes = new Routes();

// 添加一条路径到路由表中，其中冒号开头的代表路径参数
routes.add("/zoos/:zooId/animals/:animalId", "去动物园看动物");

// 在路由表里匹配具体的路径
Route route = routes.get("/zoos/BeijingZoo/animals/panda");
System.out.println(route.getItem()); // => "去动物园看动物"

// 获取路径参数
Map<String, String> pathParameterMap = route.getPathParameterMap();
System.out.println(pathParameterMap.get("zooId")); // => BeijingZoo
System.out.println(pathParameterMap.get("animalId")); // => panda
```

### 类说明（非标准UML图） ###
![输入图片说明](https://images.gitee.com/uploads/images/2020/0105/203106_a75c7c29_697458.png "routetable_uml.png")
